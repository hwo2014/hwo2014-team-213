(ns hwo2014bot.model)

(def default-acceleration 0.212345)
(def acceleration (atom default-acceleration))
(def velocity-info (atom {:v 0 :t 0 :d 0 :i 0}))
(def pieces-info (atom []))
(def turbo-info (atom {}))
(def turbos (atom 0))
(def c (atom 0.03))
(def b (atom 4))

(defn- previous-piece
  ([pieces index] (previous-piece pieces index 1))
  ([pieces index pos]
   (if (neg? (- index pos))
     (nth pieces (- (+ (count pieces) index) pos))
     (nth pieces (- index pos)))))

(defn- next-piece
  ([pieces index] (next-piece pieces index 1))
  ([pieces index pos]
   (if (>= (+ index pos) (count pieces))
     (nth pieces (- (+ index pos) (count pieces)))
     (nth pieces (+ index pos)))))

(defn previous-piece-value-sum [pieces index key]
  (->> (map (partial previous-piece pieces index) (range 1 15))
       (take-while key)
       (map key)
       (apply +)))

(defn next-piece-value-sum [pieces index key]
  (->> (map (partial next-piece pieces index) (range 1 15))
       (take-while key)
       (map key)
       (apply +)))

(defn- switch-before-curve-beginn [pieces index]
  (when (and (:angle (next-piece pieces index 2))
             (:switch (next-piece pieces index)))
    (if (pos? (next-piece-value-sum pieces (inc index) :angle))
      "Right"
      "Left")))

(defn- switch-after-curve-end [pieces index]
  (when-let [next-angle (:angle (next-piece pieces index))]
    (when (:switch (next-piece pieces index 2))
      (if (pos? next-angle) "Left" "Right"))))

(defn- change-curve-direction? [pieces index {:keys [angle]}]
  (let [prev-angle (:angle (previous-piece pieces index))
        next-angle (:angle (next-piece pieces index))]
    (and angle
         (or (and prev-angle (neg? (* angle prev-angle)))
             (and next-angle (neg? (* angle next-angle)))))))

(defn- throttle-value-for-curve-piece [{:keys [radius]} c]
  (let [throttle (float (Math/sqrt (* (- radius 20) @acceleration c)))]
    (if (>= throttle 1) 1 throttle)))

(defn- throttle-value-for-piece [piece]
  (cond
    (:radius piece) (throttle-value-for-curve-piece piece @c)
    :else 1))

(defn- throttle-value-at-start-for-piece [piece]
  (if (:length piece)
    1
    (throttle-value-for-curve-piece piece (* @c 0.65))))

(defn- distance [{:keys [length angle radius]}]
  (if length
    length
    (* (Math/toRadians (Math/abs angle)) radius)))

(defn- sum-distance-to-start [pieces index]
  (let [index-range (if (zero? index)
                      (range (count pieces))
                      (range 0 (dec index)))]
    (apply + (map (comp distance (partial nth pieces)) index-range))))

(defn- piece-info [pieces index piece]
  {:index index
   :angle (:angle piece)
   :radius (:radius piece)
   :length (:length piece)
   :throttle (if (:radius piece) (throttle-value-for-curve-piece piece @c) 1)
   :throttle-at-start (throttle-value-at-start-for-piece piece)
   :distance-to-start (sum-distance-to-start pieces index)
   :switch-lane (or (switch-before-curve-beginn pieces index)
                    (switch-after-curve-end pieces index))})

(defn reset-piece-info! [data]
  (let [pieces (-> data :race :track :pieces)]
    (reset! pieces-info (map-indexed (partial piece-info pieces) pieces))))

(defn- update-piece-throttle-value [piece]
  (assoc piece :throttle (throttle-value-for-piece piece)
               :throttle-at-start (throttle-value-at-start-for-piece piece)))

(defn- update-piece-throttle-values! []
  (swap! pieces-info #(map update-piece-throttle-value %)))

(defn- update-velocity-info! [tick {{:keys [pieceIndex inPieceDistance]} :piecePosition}]
  (swap! velocity-info (fn [{:keys [t d i] :as v-info}]
                         (if (or (zero? i)
                                 (and (= i pieceIndex)
                                      (> inPieceDistance d)))
                           (assoc v-info :t tick
                                         :i pieceIndex
                                         :d inPieceDistance
                                         :v (/ (- inPieceDistance d)
                                               (- tick t)))
                           (assoc v-info :i pieceIndex
                                         :d inPieceDistance)))))

(defn update-race-physics! [tick position]
  (let [{prev-v :v prev-t :t} @velocity-info
        {curr-v :v curr-t :t} (update-velocity-info! tick position)]
    (when (and (or (= @acceleration default-acceleration)
                   (< @acceleration 0.1)
                   (> @acceleration 0.3))
               (pos? (- curr-v prev-v)))
      (println "a:" (reset! acceleration (/ (- curr-v prev-v)
                                            (- curr-t prev-t))))
      (println "pieces:"(update-piece-throttle-values!)))
    nil))

(defn- exceeds-max-angle? [slip-angle angle]
  (> (+ (Math/abs slip-angle)
        (Math/abs angle))
     90))

(defn- final-velocity->distance [throttle]
  (Math/abs (/ (- (:v @velocity-info) (* throttle 10))
               (* @acceleration @c @b))))

(defn- in-track-distance [{{distance :inPieceDistance index :pieceIndex} :piecePosition}]
  (+ (:distance-to-start (nth @pieces-info index)) distance))

(defn- distance-diff [position {:keys [distance-to-start]}]
  (let [distance (in-track-distance position)]
    (if (< distance distance-to-start)
      (- distance-to-start distance)
      (+ (- (:distance-to-start (first @pieces-info)) distance)
         distance-to-start))))

(defn- slow-down-in-piece? [piece]
  (and (= (:i @velocity-info) (:index piece))
       (< (:throttle piece) (/ (:v @velocity-info) 10))))

(defn- slow-down-before-piece? [position piece]
  (and (< (:throttle-at-start piece)
          (/ (:v @velocity-info) 10))
       (not= (:i @velocity-info) (:index piece))
       (< (distance-diff position piece)
          (final-velocity->distance (:throttle-at-start piece)))))

(defn- slow-down? [index position]
  (some #(let [piece (next-piece @pieces-info index %)]
          (or (slow-down-in-piece? piece)
              (slow-down-before-piece? position piece)))
        (range (count @pieces-info))))

(defn speed-up? [index slip-angle]
  (let [{:keys [angle throttle] :as piece} (nth @pieces-info index)]
    (and angle
         (or (< (/ (:v @velocity-info) 10) throttle)
             (change-curve-direction? @pieces-info index piece)
             (> 60 (+ (Math/abs angle) (Math/abs slip-angle)))))))

(defn- is-in-front? [position-1 position-2]
  (and (= (-> position-1 :piecePosition :pieceIndex)
          (-> position-2 :piecePosition :pieceIndex))
       (= (-> position-1 :piecePosition :lane)
          (-> position-2 :piecePosition :lane))
       (< (in-track-distance position-1)
          (in-track-distance position-2))))

(defn- car-in-front? [my-position positions]
  (some (partial is-in-front? my-position) positions))

(defn- car-entering-curve-in-front? [my-position positions]
  (some #(and (is-in-front? my-position %)
              (:angle (nth @pieces-info (-> my-position
                                            :piecePosition
                                            :pieceIndex)))) positions))

(defn throttle-value [{{index :pieceIndex} :piecePosition slip-angle :angle :as position} positions]
  (let [{:keys [throttle angle]} (nth @pieces-info index)]
    (cond
      (car-entering-curve-in-front? position positions) 1
      (slow-down? index position)                       0.1
      (speed-up? index slip-angle)                      1
      (and angle (exceeds-max-angle? slip-angle angle)) 0.1
      :else throttle)))

(defn set-turbo-info! [{:keys [turboDurationTicks turboFactor]}]
  (reset! turbo-info {:duration turboDurationTicks :factor turboFactor}))

(defn inc-turbos! []
  (swap! turbos inc))

(defn dec-turbos! []
  (swap! turbos #(if (<= % 1) 0 (dec %))))

(defn turbo-possible? [{{index :pieceIndex} :piecePosition :as position} positions]
  (let [{:keys [length]} (nth @pieces-info index)
        {:keys [duration factor]} @turbo-info]
    (and length
         duration
         (pos? @turbos)
         (or (car-in-front? position positions)
             (< (* duration factor 5)
                (next-piece-value-sum @pieces-info index :length))))))

(defn decrease-c []
  (println "c updated:" (swap! c * 0.95))
  (println "b updated:" (swap! b * 0.9))
  (println "pieces updated:" (update-piece-throttle-values!)))

(defn increase-c []
  (println "c updated:" (swap! c * 1.1))
  (println "b updated:" (swap! b * 1.05))
  (println "pieces updated:" (update-piece-throttle-values!)))
