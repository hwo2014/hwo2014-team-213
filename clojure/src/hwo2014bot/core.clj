(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.model :as model])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(def my-car (atom nil))
(def last-piece-index (atom nil))
(def crashes-in-lap (atom 0))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn- my-car-data [data]
  (some #(when (= (:id %) @my-car) %) data))

(def ping-message
  {:msgType "ping" :data "ping"})

(defn- throttle-msg [data]
  {:msgType "throttle" :data data})

(defn- turbo-msg []
  (model/dec-turbos!)
  (println "Adios amigos!")
  {:msgType "turbo" :data "Adios amigos!"})

(defn- msg-for-new-piece [{{index :pieceIndex} :piecePosition :as position} positions]
  (let [{:keys [switch-lane]} (nth @model/pieces-info index)]
    (cond
      (model/turbo-possible? position positions) (turbo-msg)
      switch-lane {:msgType "switchLane" :data switch-lane}
      :else (throttle-msg (model/throttle-value position positions)))))

(defn- msg-for-same-piece [tick position positions]
  (model/update-race-physics! tick position)
  (throttle-msg (model/throttle-value position positions)))

(defmulti handle-msg :msgType)

(defmethod handle-msg "yourCar" [{:keys [data]}]
  (reset! my-car data)
  ping-message)

(defmethod handle-msg "carPositions" [{:keys [data gameTick]}]
  (let [position (my-car-data data)
        new-index? (not= @last-piece-index
                         (reset! last-piece-index (-> position :piecePosition :pieceIndex)))]
    (cond
      (not gameTick)   ping-message
      (not new-index?) (msg-for-same-piece gameTick position data)
      :else            (msg-for-new-piece position data))))

(defmethod handle-msg "turboEnd" [{:keys [data]}]
  (if (= data @my-car)
    (throttle-msg 0.1)
    ping-message))

(defmethod handle-msg "gameInit" [{:keys [data]}]
  (model/reset-piece-info! data)
  (throttle-msg 1))

(defmethod handle-msg "spawn" [{:keys [data]}]
  (if (= data @my-car)
    (throttle-msg 1)
    ping-message))

(defmethod handle-msg "crash" [{:keys [data]}]
  (if (= data @my-car)
    (do
      (swap! crashes-in-lap inc)
      (model/decrease-c)
      (throttle-msg 1))
    ping-message))

(defmethod handle-msg "lapFinished" [{:keys [data]}]
  (when (= (:car data) @my-car)
    (when (and (zero? @crashes-in-lap)
               (< (-> data :lapTime :lap) 2))
      (model/increase-c))
    (reset! crashes-in-lap 0)
    (println (:lapTime data)))
    ping-message)

(defmethod handle-msg "turboAvailable" [{:keys [data]}]
  (model/set-turbo-info! data)
  (model/inc-turbos!)
  ping-message)

(defmethod handle-msg :default [_]
  ping-message)

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))

(defn run []
  (-main "hakkinen.helloworldopen.com" "8091" "walterrw" "igKEld4HMZkP2Q"))
